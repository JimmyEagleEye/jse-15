package ru.korkmasov.tsc.model;

import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.api.entity.IWBS;

import java.util.UUID;
import java.util.Date;

public class Project implements IWBS {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    public String getId() {
        return id;
    }

    private Status status = Status.NOT_STARTED;

    private Date startDate;

    private Date finishDate;

    private Date created = new Date();

    public Project() { }

    public Project(String name) { this.name = name; }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() { return description; }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() { return status; }

    public void setStatus(Status status) { this.status = status; }

    public Date getStartDate() { return startDate; }

    public Date getFinishDate() { return finishDate; }

    public Date getCreated() { return created; }

    public void setStartDate(Date startDate) { this.startDate = startDate; }

    public void setFinishDate(Date finishDate) { this.finishDate = finishDate; }

    public void setCreated(Date created) { this.created = created; }

    @Override
    public String toString() {
        return id + ": " + name;
    }
}
