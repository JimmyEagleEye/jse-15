package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.util.TerminalUtil;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;

import java.util.List;

public interface ITaskController {

    void showList();

    void showById() throws TaskNotFoundException, EmptyIdException;

    void showByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException;

    void showByName() throws TaskNotFoundException, EmptyNameException, EmptyIdException;

    void create() throws TaskNotFoundException, EmptyNameException;

    Task add(String name, String description);

    void clear();

    void removeById() throws TaskNotFoundException, EmptyIdException;

    void removeByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException;

    void removeByName() throws TaskNotFoundException, EmptyNameException;

    void updateByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException, EmptyNameException;

    void updateById() throws TaskNotFoundException, EmptyIdException, EmptyNameException;

    void startById() throws TaskNotFoundException, EmptyIdException;

    void startByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException;

    void startByName() throws TaskNotFoundException, EmptyNameException;

    void finishById() throws TaskNotFoundException, EmptyIdException;

    void finishByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException;

    void finishByName() throws TaskNotFoundException, EmptyNameException;

    void findAllTaskByProjectId() throws ProjectNotFoundException;

    void bindTaskToProjectById() throws TaskNotFoundException, ProjectNotFoundException, EmptyIdException;

    void unbindTaskById() throws TaskNotFoundException, EmptyIdException;

}
