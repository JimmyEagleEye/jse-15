package ru.korkmasov.tsc;

import ru.korkmasov.tsc.bootstrap.Bootstrap;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.system.UnknownArgumentException;

public class Application {

    public static void main(final String[] args) throws UnknownArgumentException, EmptyNameException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
