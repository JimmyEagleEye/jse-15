package ru.korkmasov.tsc.exception.system;

public class UnknownCommandException extends Exception {

    public UnknownCommandException() {
        super("Error! Unknown command...");
    }

}
