package ru.korkmasov.tsc.exception.system;

public class IndexIncorrectException extends Exception{

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(final String value) {
        super("Error! This value ``" + value + "`` is not number...");
    }

    public IndexIncorrectException() {
        super("Error! Incorrect Index...");
    }

}
