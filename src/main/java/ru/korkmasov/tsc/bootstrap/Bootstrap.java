package ru.korkmasov.tsc.bootstrap;

import ru.korkmasov.tsc.api.*;
import ru.korkmasov.tsc.controller.*;
import ru.korkmasov.tsc.repository.*;
import ru.korkmasov.tsc.service.*;
import ru.korkmasov.tsc.model.*;
import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.constant.ArgumentConst;
import ru.korkmasov.tsc.constant.TerminalConst;
import ru.korkmasov.tsc.util.TerminalUtil;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.exception.system.UnknownArgumentException;
import ru.korkmasov.tsc.exception.system.UnknownCommandException;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController((ICommandService) commandService);
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);
    private final ITaskController taskController = new TaskController(taskService, projectTaskService);
    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    public void run(final String[] args) throws UnknownArgumentException, EmptyNameException {
        //displayWelcome();
        System.out.println("** WELCOME TO TASK MANAGER **");
        initData();
        if (parseArgs(args)) System.exit(0);
        process();
    }

    private void initData() throws EmptyNameException {
        projectService.add("Project 1", "-").setStatus(Status.NOT_STARTED);
        projectService.add("Project 2", "-").setStatus(Status.IN_PROGRESS);
        projectService.add("Project 3", "-").setStatus(Status.COMPLETED);
        projectService.add("Project 4", "-").setStatus(Status.COMPLETED);
    }

    public boolean parseArgs(String[] args) throws UnknownArgumentException {
        if(args == null || args.length == 0);
        if (args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseArg(final String arg) throws UnknownArgumentException {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ARG_ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                throw new UnknownArgumentException();
        }
    }

    public void parseCommand(final String command) throws IndexIncorrectException, TaskNotFoundException, ProjectNotFoundException, UnknownCommandException, EmptyIdException, EmptyNameException, EmptyIndexException {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.CMD_TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.CMD_TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.CMD_TASK_SHOW_BY_ID:
                taskController.showById();
                break;
            case TerminalConst.CMD_TASK_SHOW_BY_INDEX:
                taskController.showByIndex();
                break;
            case TerminalConst.CMD_TASK_SHOW_BY_NAME:
                taskController.showByName();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_NAME:
                taskController.removeByName();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case TerminalConst.CMD_PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.CMD_PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.CMD_TASK_START_BY_ID:
                taskController.startById();
                break;
            case TerminalConst.CMD_TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case TerminalConst.CMD_TASK_START_BY_NAME:
                taskController.startByName();
                break;
            case TerminalConst.CMD_TASK_FINISH_BY_ID:
                taskController.finishById();
                break;
            case TerminalConst.CMD_TASK_FINISH_BY_INDEX:
                taskController.finishByIndex();
                break;
            case TerminalConst.CMD_TASK_FINISH_BY_NAME:
                taskController.finishByName();
                break;
            case TerminalConst.CMD_TASK_BIND_BY_ID:
                taskController.bindTaskToProjectById();
                break;
            case TerminalConst.CMD_TASK_UNBIND_BY_ID:
                taskController.unbindTaskById();
                break;
            case TerminalConst.CMD_TASK_FIND_BY_PROJECT_ID:
                taskController.findAllTaskByProjectId();
                break;
            case TerminalConst.CMD_PROJECT_CLEAR:
                projectController.clear();
                break;
            case TerminalConst.CMD_PROJECT_SHOW_BY_ID:
                projectController.showById();
                break;
            case TerminalConst.CMD_PROJECT_SHOW_BY_INDEX:
                projectController.showByIndex();
                break;
            case TerminalConst.CMD_PROJECT_SHOW_BY_NAME:
                projectController.showByName();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeById();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeByIndex();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_NAME:
                projectController.removeByName();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_NAME:
                projectController.startByName();
                break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_ID:
                projectController.finishById();
                break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_INDEX:
                projectController.finishByIndex();
                break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_NAME:
                projectController.finishByName();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            default:
                throw new UnknownCommandException();
        }
    }

    public void displayError() {
        System.out.println("Incorrect command. Use " + TerminalConst.CMD_HELP + " for display list of terminal commands.");
    }

    public void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            try {
                commandController.displayWait();
                command = TerminalUtil.nextLine();
                parseCommand(command);
                //System.err.println("[OK]");
            }
            catch(Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

}
