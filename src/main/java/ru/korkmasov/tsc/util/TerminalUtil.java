package ru.korkmasov.tsc.util;

import java.util.Scanner;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;

public class TerminalUtil {

    static Scanner SCANNER = new Scanner(System.in);

    public static String nextLine(){
        return SCANNER.nextLine();
    }

    public static Integer nextNumber() throws IndexIncorrectException {
        final String value = SCANNER.nextLine();
        try {
        return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IndexIncorrectException(value);
        }

    }

}
